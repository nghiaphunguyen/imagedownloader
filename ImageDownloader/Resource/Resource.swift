//
//  Resource.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift

enum ResourceStatusRaw: Int {
    case idle, downloading, paused, finished
}

enum ResourceStatus {
    case idle, downloading(Float), paused, finished
    
    var statusRaw: ResourceStatusRaw {
        switch self {
        case .idle:
            return . idle
        case .downloading(_):
            return .downloading
        case .paused:
            return .paused
        case .finished:
            return .finished
        }
    }
    
    static var stateFlow: [ResourceStatusRaw: [ResourceStatusRaw]] {
        return [.idle: [.downloading, .paused, .finished],
                .downloading: [.idle, .paused, .downloading],
                .paused: [.downloading],
                .finished: [.downloading]]
    }
    
    func changeState(state: ResourceStatus) -> ResourceStatus {
        if (ResourceStatus.stateFlow[state.statusRaw]?.contains(self.statusRaw) ?? false) == true {
            return state
        }
        
        return self
    }
}

protocol IResource {
    var resourceStatusObservable: Observable<ResourceStatus> { get }
    var downloadees: [IDownload] {get}
    var progress: Float {get}
    var state: ResourceStatus {get}
    var name: String {get}
    
    func start()
    func pause()
    func resume()
    func cancel()
}

class Resource: NSObject {
    fileprivate let rx_resourceStatus = Variable<ResourceStatus>(.idle)
    fileprivate let rx_numFileDownloaded = Variable<Int>(0)
    
    fileprivate(set) var state: ResourceStatus {
        get {
            return self.rx_resourceStatus.value
        }
        set {
            self.rx_resourceStatus.value = newValue
        }
    }
    
    var progress: Float {
        get {
            return Float(self.rx_numFileDownloaded.value) / Float(self.downloadees.count)
        }
    }
    
    let downloadees: [IDownload]
    let name: String
    init(name: String, downloadees: [IDownload]) {
        self.name = name
        self.downloadees = downloadees
        super.init()
        
        Observable.from(self.downloadees.map {$0.downloadStatusObservable.filter({$0.statusRaw == .finished})}).merge().observeOn(MainScheduler.instance).bindNext { (status) in
            self.rx_numFileDownloaded.value = self.rx_numFileDownloaded.value + 1
        }.addDisposableTo(self.nk_disposeBag)
        
        self.rx_resourceStatus.asObservable().filter {$0.statusRaw == .idle} .bindNext { _ in
            self.rx_numFileDownloaded.value = 0
        }.addDisposableTo(self.nk_disposeBag)
        
        self.rx_numFileDownloaded.asObservable().filter {$0 > 0}.bindNext { _ in
            self.state = .downloading(self.progress)
        }.addDisposableTo(self.nk_disposeBag)
        
        self.rx_numFileDownloaded.asObservable().filter {$0 == self.downloadees.count}.bindNext { _ in
            self.state = .finished
        }.addDisposableTo(self.nk_disposeBag)
    }
}

extension Resource: IResource {
    var resourceStatusObservable: Observable<ResourceStatus> {
        return self.rx_resourceStatus.asObservable()
    }
    
    func start() {
        guard self.state.statusRaw == .idle else { return }
        
        for downloadee in self.downloadees {
            downloadee.start()
        }
        
        self.state = self.state.changeState(state: .downloading(self.progress))
    }
    
    func pause() {
        guard self.state.statusRaw == .downloading else { return }
        
        for downloadee in self.downloadees {
            downloadee.pause()
        }
        
        self.state = self.state.changeState(state: .paused)
    }
    
    func resume() {
        guard self.state.statusRaw == .paused else { return }
        
        for downloadee in self.downloadees {
            downloadee.resume()
        }
        
        self.state = self.state.changeState(state: .downloading(self.progress))
    }
    
    func cancel() {
        guard self.state.statusRaw != .idle else { return }
        
        for downloadee in self.downloadees {
            downloadee.cancel()
        }
        
        self.state = self.state.changeState(state: .idle)
    }
}
