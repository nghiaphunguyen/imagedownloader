//
//  Image+NKit.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import NRxSwift

enum ImageType {
    case png
    case jpg(CGFloat)
}

extension UIImage {
    func save(in url: URL, imageType: ImageType = .png, options: Data.WritingOptions = .atomic) -> Observable<URL> {
        
        return Observable.nk_baseCreate({ (observer) in
            do {
                switch imageType {
                case .png:
                    try UIImagePNGRepresentation(self)?.write(to: url, options: options)
                case .jpg(let quality):
                    try UIImageJPEGRepresentation(self, quality)?.write(to: url, options: options)
                }
                
                observer.nk_setValue(url)
            } catch (let error) {
                observer.nk_setError(error)
            }
        })
        
        
    }
}
