//
//  Unzip+NKit.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import Zip
import RxSwift
import NRxSwift
import NLogProtocol

extension Zip {
    static func unzip(with source: URL, destination: URL) -> Observable<URL> {
        return Observable.nk_baseCreate({ (observer) in
            
        do {
            try Zip.unzipFile(source, destination: destination, overwrite: true, password: nil, progress: { progress in
                    if progress >= 1 {
                       observer.nk_setValue(destination)
                }
            })
        } catch (let error) {
            NKLog.debug("Can not unzip file url: \(source.absoluteString) error: \(error)")
            observer.nk_setError(error)
        }
        })
    }
}
