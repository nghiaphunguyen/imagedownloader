//
//  FileManager+NKit.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import NLogProtocol

extension FileManager {
    static func copyData(from source: URL, to destination: URL) throws -> URL {
        let fileManager = FileManager.init()
        try? fileManager.removeItem(at: destination)
        try fileManager.copyItem(at: source, to: destination)
        
        return destination
    }
    
    static func copyDataObservable(from source: URL, to destination: URL) -> Observable<URL> {
        return Observable.nk_baseCreate { (observer) in
            do {
                _ = try self.copyData(from: source, to: destination)
                observer.nk_setValue(destination)
            } catch (let error) {
                NKLog.debug("Can not copy file source: \(source.absoluteString) destination: \(destination.absoluteString) error: \(error)")
                observer.nk_setError(error)
            }
        }
    }

    static func files(from dir: URL) -> [URL] {
        let fileManager = FileManager.init()
        
        guard fileManager.isDir(dir: dir) else {
            return [URL]()
        }
        
        let urls = (try? fileManager.contentsOfDirectory(at: dir, includingPropertiesForKeys: nil, options: [FileManager.DirectoryEnumerationOptions.skipsHiddenFiles])) ?? [URL]()
        
        return urls.map {FileManager.files(from: $0)}.reduce(urls) {
            $0 + $1
        }
    }
}

extension FileManager {
    func isDir(dir: URL) -> Bool {
        var isDir: ObjCBool = false
        let result = self.fileExists(atPath: dir.path, isDirectory: &isDir)
        return result && isDir.boolValue
    }
}
