//
//  Downloader
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/9/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import NKit

protocol Downloadable {
    var downloadTaskModelObservable: Observable<DownloadTaskModel> {get}
    func downloadTask(with url: URL) -> URLSessionDownloadTask
    func downloadTaskWithResumeData(_ resumeData: Data) -> URLSessionDownloadTask
}

class Downloader: NSObject {
    fileprivate var urlSession: URLSession!
    
    fileprivate lazy var rx_downloadTaskModel = Variable<DownloadTaskModel?>(nil)
    
    init(configuration: URLSessionConfiguration = .default) {
        super.init()
        self.urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }
}

extension Downloader: Downloadable {
    var downloadTaskModelObservable: Observable<DownloadTaskModel> {
        return self.rx_downloadTaskModel.asObservable().nk_unwrap()
    }
    
    func downloadTask(with url: URL) -> URLSessionDownloadTask {
        return self.urlSession.downloadTask(with: url)
    }
    
    func downloadTaskWithResumeData(_ resumeData: Data) -> URLSessionDownloadTask {
        return self.urlSession.downloadTask(withResumeData: resumeData)
    }
}

extension Downloader: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        self.rx_downloadTaskModel.value = DownloadTaskModel(urlSessionTask: downloadTask, status: .downloading(CGFloat(bytesWritten) / CGFloat(totalBytesExpectedToWrite)))
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        self.rx_downloadTaskModel.value = DownloadTaskModel(urlSessionTask: downloadTask, status: .downloading(CGFloat(fileOffset) / CGFloat(expectedTotalBytes)))
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        self.rx_downloadTaskModel.value = DownloadTaskModel(urlSessionTask: downloadTask, status: .finish(location))
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let error = error else {return}
        
        let nserror = error as NSError
        if nserror.domain == "NSURLErrorDomain" && nserror.code == -999 {
            return
        }
        
        self.rx_downloadTaskModel.value = DownloadTaskModel(urlSessionTask: task, status: .error(error))
    }
}
