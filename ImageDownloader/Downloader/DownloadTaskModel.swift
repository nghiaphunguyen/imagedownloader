//
//  DownloadTaskModel.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/9/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import Foundation
import UIKit

struct DownloadTaskModel {
    let urlSessionTask: URLSessionTask
    let status: DownloadTaskStatus
}

enum DownloadTaskStatus {
    case error(Error)
    case finish(URL)
    case downloading(CGFloat)
    
}
