//
//  Downloadee.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/9/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import Foundation
import RxSwift
import NKit
import NLogProtocol

enum DownloadStatusRaw: Int {
    case idle, downloading, pausing, paused, resuming, finished, processing, error
}

enum DownloadStatus {
    case idle, downloading(CGFloat), pausing, paused, resuming, finished(Any), processing(String), error(Error)
    
    var statusRaw: DownloadStatusRaw {
        switch self {
        case .idle :
            return .idle
        case .downloading(_):
            return .downloading
        case .pausing:
            return .pausing
        case .paused:
            return .paused
        case .resuming:
            return .resuming
        case .finished(_):
            return .finished
        case .processing(_):
            return .processing
        case .error(_):
            return .error
        }
    }
    
    static var stateFlow: [DownloadStatusRaw: [DownloadStatusRaw]] {
        return [.idle: [.finished, .error, .downloading, .pausing, .paused, .processing],
                .downloading: [.idle, .resuming, .downloading],
                .pausing: [.downloading],
                .paused: [.pausing],
                .resuming: [.paused],
                .finished: [.processing, .downloading],
                .processing: [.downloading],
                .error: [.downloading, .pausing, .resuming, .processing]
        ]
    }
    
    func changeState(state: DownloadStatus) -> DownloadStatus {
        if (DownloadStatus.stateFlow[state.statusRaw]?.contains(self.statusRaw) ?? false) == true {
            return state
        }
        
        return self
    }
}

protocol IDownload {
    var url: URL {get}
    var status: DownloadStatus {get}
    
    var downloadStatusObservable: Observable<DownloadStatus> {get}
    
    func start()
    func resume()
    func startOrResume()
    func pause()
    func cancel()
    
}

extension IDownload {
    var errorObservable: Observable<Error> {
        return self.downloadStatusObservable.filter {$0.statusRaw == .error}.map { (status) -> Error? in
            switch status {
            case .error(let error):
                return error
            default:
                return nil
            }
        }.nk_unwrap()
    }
    
    var resultObservable: Observable<Any> {
        return self.downloadStatusObservable.filter {$0.statusRaw == .finished}.map { (status) -> Any? in
            switch status {
            case .finished(let result):
                return result
            default:
                return nil
            }
            }.nk_unwrap()
    }
    
    var progressObservable: Observable<CGFloat> {
        return self.downloadStatusObservable.filter {$0.statusRaw == .downloading}.map { (status) -> CGFloat? in
            switch status {
            case .downloading(let progress):
                return progress
            default:
                return nil
            }
            }.nk_unwrap()
    }
}

class Downloadee: NSObject {
    fileprivate let rx_downloadStatus = Variable<DownloadStatus>(.idle)
    
    let url: URL
    
    fileprivate let converter: Convertable?
    
    fileprivate var downloadTask: URLSessionDownloadTask?
    
    fileprivate var downloader: Downloadable?
    
    fileprivate var resumeData: Data? = nil
    
    init(url: URL, downloader: Downloadable, converter: Convertable? = nil) {
        self.converter = converter
        self.downloader = downloader
        self.url = url
        super.init()
        
        self.downloader?.downloadTaskModelObservable
            .filter { $0.urlSessionTask.taskIdentifier == self.downloadTask?.taskIdentifier }
            .bindNext { (model) in
                switch model.status {
                case .error(let error) :
                    NKLog.debug("Occure error - downloadTask: \(self.downloadTask?.taskIdentifier) error: \(error)")
                    self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .error(error))
                case .downloading(let progress):
                    self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .downloading(progress))
                case .finish(let location):
                    NKLog.debug("finished - downloadTask: \(self.downloadTask?.taskIdentifier) url: \(location.absoluteString)")
                    
                    let url = URL(fileURLWithPath: nk_documentDirectory ++ self.url.lastPathComponent.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)
                    do {
                        _ = try FileManager.copyData(from: location, to: url)
                    } catch (let error) {
                        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .error(error))
                        return
                    }
                    
                    if let converter = self.converter {
                        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .processing(converter.processingDescription))
                        self.process(url: url)
                    } else {
                        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .finished(url))
                    }
                }
                
            }.addDisposableTo(self.nk_disposeBag)
    }
    
    private func process(url: URL) {
        self.converter?.convert(url: url).subscribe(onNext: { (result) in
            self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .finished(result))
        }, onError: { (error) in
            self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .error(error))
        }).addDisposableTo(self.nk_disposeBag)
    }
}

extension Downloadee {
    static func create(from urlString: String, downloader: Downloadable) -> Downloadee? {
        guard let url = URL(string: urlString) else {return nil}
        
        let converter: Convertable
        switch url.pathExtension {
        case "pdf":
            converter = ImagePDFConverter()
        case "zip":
            converter = ImageUnzipConverter()
        default:
            converter = DefaultConverter()
        }
        
        return Downloadee(url: url, downloader: downloader, converter: converter)
    }
}

extension Downloadee: IDownload {
    var status: DownloadStatus {
        return self.rx_downloadStatus.value
    }
    
    var downloadStatusObservable: Observable<DownloadStatus> {
        return self.rx_downloadStatus.asObservable()
    }
    
    func start() {
        if self.rx_downloadStatus.value.statusRaw == .finished
        || self.rx_downloadStatus.value.statusRaw == .error {
            self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .idle)
        }
        
        guard self.rx_downloadStatus.value.statusRaw == .idle else {
            NKLog.debug("Can't start downloadTask: \(self.downloadTask?.taskIdentifier) status:\(self.rx_downloadStatus.value.statusRaw)")
            return
        }
        
        self.downloadTask = self.downloader?.downloadTask(with: url)
        self.resumeData = nil
        self.downloadTask?.resume()
        
        NKLog.debug("Started downloadTask: \(self.downloadTask?.taskIdentifier)")
        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .downloading(0))
    }
    
    func pause() {
        guard self.rx_downloadStatus.value.statusRaw == .downloading else {
            NKLog.debug("Can't pause downloadTask: \(self.downloadTask?.taskIdentifier) status: \(self.rx_downloadStatus.value.statusRaw)")
            
            return
        }
        
        self.downloadTask?.cancel { (data) in
            self.resumeData = data
            
            NKLog.debug("Paused downloadTask: \(self.downloadTask?.taskIdentifier)")
            self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .paused)
        }
        
        NKLog.debug("Pausing downloadTask: \(self.downloadTask?.taskIdentifier)")
        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .pausing)
    }
    
    func cancel() {
        guard self.rx_downloadStatus.value.statusRaw != .idle else {
                NKLog.debug("Can't cancel downloadTask: \(self.downloadTask?.taskIdentifier) status:\(self.rx_downloadStatus.value.statusRaw)")
                return
        }
        
        self.downloadTask?.cancel()
        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .idle)
        NKLog.debug("Cancelled downloadTask: \(self.downloadTask?.taskIdentifier)")
    }
    
    func resume() {
        guard let downloader = self.downloader,
            self.rx_downloadStatus.value.statusRaw == .paused else {
                NKLog.debug("Can't resume downloadTask: \(self.downloadTask?.taskIdentifier) status:\(self.rx_downloadStatus.value.statusRaw)")
                return
        }
        
        if let data = self.resumeData {
            self.downloadTask = downloader.downloadTaskWithResumeData(data)
        } else {
            self.downloadTask = downloader.downloadTask(with: self.url)
        }
        
        self.downloadTask?.resume()
        
        self.rx_downloadStatus.value = self.rx_downloadStatus.value.changeState(state: .resuming)
        NKLog.debug("Resuming downloadTask: \(self.downloadTask?.taskIdentifier)")
    }
    
    func startOrResume() {
        self.rx_downloadStatus.value.statusRaw == .paused ? self.resume() : self.start()
    }
}
