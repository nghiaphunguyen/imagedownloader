//
//  PDFConverter.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import NKit

typealias ImagePDFConverterOuput = URL

struct ImagePDFConverter: Convertable {
    enum ConvertError: Error {
        case canNotCreateDocument
        case invalidPage
    }
    
    func convert(url: URL) -> Observable<Any> {
        let path = nk_documentDirectory ++ (url.deletingPathExtension().lastPathComponent + ".png") //TODO: check again
        let destinationURL = URL.init(fileURLWithPath: path)
        
        return self.pdfToImage(from: url).flatMapLatest{$0.save(in: destinationURL)}.map {$0 as Any}
        
    }
    
    private func pdfToImage(from url: URL) -> Observable<UIImage> {
        return Observable.nk_baseCreate({ (observer) in
            guard let document = CGPDFDocument(url as CFURL) else {
                observer.nk_setError(ConvertError.canNotCreateDocument)
                return
            }
            
            //TODO: concat all pages
            guard let page = document.page(at: 1) else {
                observer.nk_setError(ConvertError.invalidPage)
                return
            }
            
            let pageRect = page.getBoxRect(.mediaBox)
            let renderer = UIGraphicsImageRenderer(size: pageRect.size)
            let image = renderer.image(actions: { (context) in
                UIColor.white.set()
                context.fill(pageRect)
                
                context.cgContext.translateBy(x: 0, y: pageRect.size.height)
                context.cgContext.scaleBy(x: 1, y: -1)
                
                context.cgContext.drawPDFPage(page)
            })
            
            observer.nk_setValue(image)
        })
    }
}
