//
//  Processor.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift

protocol Convertable {
    var processingDescription: String {get}
    func convert(url: URL) -> Observable<Any>
}

extension Convertable {
    var processingDescription: String {
        return "Processing"
    }
}
