//
//  ImageUnzipConverter.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit
import NRxSwift
import RxSwift
import Zip

typealias ImageUnzipConverterOuput = URL

struct ImageUnzipConverter: Convertable {
    enum ConvertError: Error {
        case noImageExist
    }
    
    var processingDescription: String {
        return "unzip"
    }
    
    func convert(url: URL) -> Observable<Any> {
        let destination = URL.init(fileURLWithPath: url.deletingPathExtension().path)
        
        return Zip.unzip(with: url, destination: destination)
            .flatMapLatest({self.imageURL(from: $0)})
            .map {$0 as Any}
    }
    
    private func imageURL(from dir: URL) -> Observable<URL> {
        func filter(fileUrl: URL) -> Bool {
            return Constant.ImageType(rawValue: fileUrl.pathExtension)
                .flatMap({Constant.ImageType.all.contains($0)}) == true
        }
        
        return Observable.nk_baseCreate({ (observer) in
            guard let url = FileManager.files(from: dir)
                .filter(filter).first else {
                observer.nk_setError(ConvertError.noImageExist)
                return
            }
            
            observer.nk_setValue(url)
        })
    }
}
