//
//  DefaultConverter.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import NRxSwift
import NKit

typealias DefaultConverterOuput = URL

struct DefaultConverter: Convertable {
    func convert(url: URL) -> Observable<Any> {
        return Observable.just(url)
    }    
}
