//
//  JSONSUnzipConverter.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON
import NKit
import Zip

typealias JSONSUnzipConverterOutput = [(String, JSON)]

struct JSONSUnzipConverter: Convertable {
    func convert(url: URL) -> Observable<Any> {
        let destination = URL(fileURLWithPath: url.deletingPathExtension().path)
        return Zip.unzip(with: url, destination: destination)
            .map {FileManager.files(from: $0)
                .filter { !FileManager().isDir(dir: $0)}
                .filter { $0.pathExtension == "json"}
                .flatMap({ url -> (String, JSON)? in
                    guard let data = try? Data.init(contentsOf: url) else {
                        return nil
                    }
                    
                    return (url.deletingPathExtension().lastPathComponent, JSON.init(data: data))
                })
            }
            .map {$0 as Any}
    }
}
