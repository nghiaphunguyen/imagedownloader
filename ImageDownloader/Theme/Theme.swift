//
//  Theme.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/11/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit

protocol Theme {
    var titleSize: CGFloat {get}
    var normalSize: CGFloat {get}
    var subtitleSize: CGFloat {get}
    var smallSize: CGFloat {get}
    
    var titleFont: UIFont {get}
    var normalFont: UIFont {get}
    var subtitleFont: UIFont {get}
    var smallFont: UIFont {get}
    
    var smallMargin: CGFloat {get}
}

var THEME: Theme = MainTheme()

struct MainTheme: Theme {
    var titleSize: CGFloat {
        return 16
    }
    var normalSize: CGFloat {
        return 14
    }
    var subtitleSize: CGFloat {
        return 14
    }
    var smallSize: CGFloat {
        return 12
    }
    
    var titleFont: UIFont {
        return UIFont.systemFont(ofSize: self.titleSize)
    }
    
    var normalFont: UIFont {
        return UIFont.systemFont(ofSize: self.normalSize)
    }
    
    var subtitleFont: UIFont {
        return UIFont.systemFont(ofSize: self.subtitleSize)
    }
    
    var smallFont: UIFont {
        return UIFont.systemFont(ofSize: self.smallSize)
    }
    
    var smallMargin: CGFloat {
        return 5
    }
}
