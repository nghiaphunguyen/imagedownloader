//
//  MainStyle.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/11/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NStyle

struct Class {
    enum Label: String, NKStringConvertible {
        case title
        case subtitle
        case normal
        case small
    }
    
    enum Navigation: String, NKStringConvertible {
        case white
        case gray
    }
    
    enum Progress: String, NKStringConvertible {
        case gray
    }
    
    enum Button: String, NKStringConvertible {
        case plain
    }
}

struct MainStyle: NKStylesheet {
    
    func build(stylist: NKStylist) {
        stylist.registerClass(name: Class.Label.title) { (label: UILabel) in
            label.textAlignment = .left
            label.textColor = UIColor.black
            label.font = UIFont.systemFont(ofSize: 16)
            label.sizeToFit()
        }
        
        stylist.registerClass(name: Class.Label.subtitle) { (label: UILabel) in
            label.textAlignment = .left
            label.textColor = UIColor.gray
            label.font = UIFont.systemFont(ofSize: 14)
            label.sizeToFit()
        }
        
        stylist.registerClass(name: Class.Label.normal) { (label: UILabel) in
            label.textAlignment = .left
            label.textColor = UIColor.black
            label.font = UIFont.systemFont(ofSize: 14)
            label.sizeToFit()
        }
        
        stylist.registerClass(name: Class.Label.small) { (label: UILabel) in
            label.textAlignment = .left
            label.textColor = UIColor.black
            label.font = UIFont.systemFont(ofSize: 12)
            label.sizeToFit()
        }
        
        stylist.registerClass(name: Class.Navigation.white) { (viewController: UIViewController) in
            viewController.nk_setTintColor(UIColor.black).nk_setBarTintColor(UIColor.white)
        }
        
        stylist.registerClass(name: Class.Progress.gray) { (progressView: UIProgressView) in
            progressView.tintColor = UIColor.gray
            progressView.trackTintColor = UIColor.clear
        }
        
        stylist.registerClass(name: Class.Button.plain) { (button: UIButton) in
            button.setTitleColor(UIColor.white, for: .normal)
            button.setBackgroundImage(UIImage.nk_fromColor(UIColor.black.withAlphaComponent(0.5)), for: .normal)
            button.titleLabel?.font = THEME.titleFont
            button.nk_cornerRadius = 3
        }
    }
}
