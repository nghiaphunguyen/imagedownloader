//
//  LogConfig.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NLog
import NKit
import NLogProtocol

struct LogConfig: NKAppConfigDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        
        #if DEBUG
//        NLog.levels = NLog.kDebugLevels
//            NKLOG = NLog.self
        #else
            NLog.levels = []
            NKLOG = nil
        #endif
        
    }
}
