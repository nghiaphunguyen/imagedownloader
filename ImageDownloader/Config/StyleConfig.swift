//
//  StyleConfig.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/11/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NStyle
import NKit
struct StyleConfig: NKAppConfigDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) {
        NKCSS.registerStyleSheets(stylesheets: MainStyle())
    }
}
