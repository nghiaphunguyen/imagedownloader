//
//  Constant.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/10/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import Foundation

struct Constant {
    enum ImageType: String {
        case jpg
        case png
        
        static var all: [ImageType] {
            return [.jpg, .png]
        }
    }
}
