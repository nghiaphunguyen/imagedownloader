//
//  ImageDetailViewModel.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit
import RxSwift

protocol ImageDetailViewModel {
    var pathObservable: Observable<String> {get}
}

class ImageDetailViewModelImp: NSObject {
    fileprivate let rx_path = Variable<String?>(nil)
    
    init(path: String?) {
        self.rx_path.value = path
        
        super.init()
    }
}

extension ImageDetailViewModelImp: ImageDetailViewModel {
    var pathObservable: Observable<String> {
        return self.rx_path.asObservable().nk_unwrap()
    }
}
