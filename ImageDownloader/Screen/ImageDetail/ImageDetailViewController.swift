//
//  ImageDetailViewController.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit

class ImageDetailViewController: UIViewController {
    enum Id: String, NKViewIdentifier {
        case imageView, scrollView, closeButton
    }
    
    lazy var imageView: UIImageView = Id.imageView.view(self)
    lazy var scrollView: UIScrollView = Id.scrollView.view(self)
    lazy var closeButton: UIButton = Id.closeButton.view(self)
    
    let model: ImageDetailViewModel
    init(model: ImageDetailViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        self.view.nk_config {
            $0.backgroundColor = UIColor.black
        }
        .nk_addSubview(UIScrollView().nk_id(Id.scrollView)) {
            $0.backgroundColor = UIColor.gray
            
            $0.snp.makeConstraints({ (make) in
                make.center.equalToSuperview()
                make.width.equalToSuperview()
                make.height.equalTo(0)
            })
            
            $0.nk_addSubview(UIImageView().nk_id(Id.imageView)) {
                $0.snp.makeConstraints({ (make) in
                    make.edges.equalToSuperview()
                    make.width.equalToSuperview()
                })
            }
        }
        .nk_addSubview(UIButton().nk_id(Id.closeButton).nk_classes(Class.Button.plain)) {
            $0.setTitle("Close", for: .normal)
            
            $0.snp.makeConstraints({ (make) in
                make.leading.bottom.equalToSuperview().inset(THEME.smallMargin)
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupRx() {
        self.model.pathObservable.bindNext { (path) in
            guard let image = UIImage.init(contentsOfFile: path) else {
                return
            }
            
            let height = image.size.height * (self.view.nk_width / image.size.width)
            
            self.scrollView.snp.remakeConstraints({ (make) in
                make.center.equalToSuperview()
                make.width.equalToSuperview()
                make.height.equalTo(min(height, NKScreenSize.Current.height))
            })
            
            self.imageView.image = image
            self.imageView.snp.remakeConstraints({ (make) in
                make.edges.equalToSuperview()
                make.width.equalToSuperview()
                make.height.equalTo(height)
            })
            
            self.view.layoutIfNeeded()
        }.addDisposableTo(self.nk_disposeBag)
        
        self.closeButton.rx.tap.bindNext {
            self.dismiss(animated: true, completion: nil)
        }.addDisposableTo(self.nk_disposeBag)
    }
}
