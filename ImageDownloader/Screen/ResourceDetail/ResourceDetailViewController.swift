//
//  ResourceDetailViewController.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift
import NKit
import NLogProtocol

class ResourceDetailViewController: UIViewController {
    
    struct Layout {
        static var Spacing: CGFloat {return 2}
        static var NumOfCellInRow: Int {return 4}
        static var CellSize: CGSize {
            let width = (NKScreenSize.Current.width - Spacing * CGFloat(NumOfCellInRow - 1)) / CGFloat(NumOfCellInRow)
            return CGSize.init(width: width, height: width)
        }
    }
    
    enum Id: String, NKViewIdentifier {
        case collectionView
    }
    
    lazy var collectionView: NKCollectionView = Id.collectionView.view(self)
    
    let model: ResourceDetailViewModel
    init(model: ResourceDetailViewModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        NKLog.debug("Cell size: \(Layout.CellSize)")
        
        self.view.nk_addSubview(NKCollectionView.init(options: [.InterItemSpacing(Layout.Spacing), .LineSpace(Layout.Spacing), .ItemSize(Layout.CellSize)]).nk_id(Id.collectionView)) {
            $0.backgroundColor = UIColor.white
            $0.nk_dataSource = self
            $0.delegate = self
            $0.registerView(ResourceDetailCollectionViewCell.self)
            
            $0.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.nk_setRightBarButton("Reload", selector: #selector(ResourceDetailViewController.reload))
        _ = self.nk_classes(Class.Navigation.white)
    }
    
    func reload() {
        self.model.reload()
    }
    
    func setupRx() {
        self.model.nameObservable.observeOn(MainScheduler.instance).bindNext { (title) in
            self.nk_setTitle(title: title, color: UIColor.black, font: THEME.titleFont)
        }.addDisposableTo(self.nk_disposeBag)
        
        self.model.resourceDetailItemViewModelsObservable.debounce(0.1, scheduler: MainScheduler.instance).observeOn(MainScheduler.instance).bindNext { models in
            //TODO: should use diff algo from IGListKit to change specific data instead effect to all data
            self.collectionView.reloadData()
        }.addDisposableTo(self.nk_disposeBag)
    }
}

extension ResourceDetailViewController: NKCollectionViewDataSource {
    func itemsForCollectionView(collectionView: NKCollectionView) -> [[Any]] {
        return [self.model.resourceDetailItemViewModels.map {$0 as Any}]
    }
}

extension ResourceDetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < self.model.resourceDetailItemViewModels.count {
            let item = self.model.resourceDetailItemViewModels[indexPath.row]
            let viewController = ImageDetailViewController.init(model: ImageDetailViewModelImp(path: item.imagePath))
            self.present(viewController, animated: true, completion: nil)
        }
    }
}
