//
//  ResourceDetailCollectionViewCell.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit

class ResourceDetailCollectionViewCell: NKBaseCollectionViewCell {
    
    enum Id: String, NKViewIdentifier {
        case imageView, statusLabel
    }
    
    fileprivate lazy var imageView: UIImageView = Id.imageView.view(self)
    fileprivate lazy var statusLabel: UILabel = Id.statusLabel.view(self)
    
    override func setupView() {
        self.nk_config {
            $0.backgroundColor = UIColor.gray
        }
        .nk_addSubview(UIImageView().nk_id(Id.imageView)) {
            $0.contentMode = .scaleAspectFill
            $0.clipsToBounds = true
            
            $0.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
        }
        .nk_addSubview(UILabel().nk_id(Id.statusLabel).nk_classes(Class.Label.small)) {
            $0.numberOfLines = 0
            $0.snp.makeConstraints({ (make) in
                make.bottom.leading.trailing.equalToSuperview().inset(THEME.smallMargin)
            })
        }
    }
}

extension ResourceDetailCollectionViewCell: NKCollectionViewItemProtocol {
    typealias CollectionViewItemModel = ResourceDetailItemViewModelImp
    
    func collectionView(collectionView: NKCollectionView, configWithModel model: ResourceDetailItemViewModelImp, atIndexPath indexPath: IndexPath) {
        if let imagePath = model.imagePath {
            self.imageView.image = UIImage.init(contentsOfFile: imagePath)
        } else {
            self.imageView.image = nil
        }
        
        self.statusLabel.text = model.status.string
        
        switch model.status {
        case .finished:
            self.backgroundColor = UIColor.clear
        default:
            self.backgroundColor = UIColor.gray
        }
    }
}

//MARK: Model

enum ResourceDetailItemViewStatus {
    case downloading(Float), queueing, error, processing(String), finished
    
    var string: String {
        switch self {
        case .downloading(let progress):
            return "Downloading \(roundf(progress * 100))%"
        case .queueing:
            return "Queueing"
        case .error:
            return "Error"
        case .processing(let processing):
            return processing
        case .finished:
            return ""
        }
    }
}

protocol ResourceDetailItemViewModel {
    var imagePath: String? {get}
    var status: ResourceDetailItemViewStatus {get}
}

struct ResourceDetailItemViewModelImp: ResourceDetailItemViewModel {
    var imagePath: String? = nil
    let status: ResourceDetailItemViewStatus
}

extension ResourceDetailItemViewModelImp {
    init(downloadeeStatus: DownloadStatus) {
        switch downloadeeStatus {
        case .downloading(let progress):
            self.status = .downloading(Float(progress))
        case .error(_):
            self.status = .error
        case .finished(let result):
            self.status = .finished
            self.imagePath = (result as? URL)?.path
        case .idle, .paused, .pausing, .resuming:
            self.status = .queueing
        case .processing(let description):
            self.status = .processing(description)
        }
    }
}
