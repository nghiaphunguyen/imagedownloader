//
//  ResourceDetailViewModel.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/12/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import RxSwift

protocol ResourceDetailViewModel {
    var resourceDetailItemViewModelsObservable: Observable<[ResourceDetailItemViewModel]> {get}
    var resourceDetailItemViewModels: [ResourceDetailItemViewModel] {get}
    func reload()
    var nameObservable: Observable<String> {get}
    
}

class ResourceDetailViewModelImp: NSObject {
    let resource: IResource
    fileprivate let rx_resourceDetailItemViewModels = Variable<[ResourceDetailItemViewModel]>([])
    
    init(resource: IResource) {
        self.resource = resource
        
        super.init()
        
        Observable.from(self.resource.downloadees.map { $0.downloadStatusObservable }).merge().observeOn(MainScheduler.instance).map { _ in
            return self.resource.downloadees.map { ResourceDetailItemViewModelImp.init(downloadeeStatus: $0.status) }
        }.bindTo(self.rx_resourceDetailItemViewModels).addDisposableTo(self.nk_disposeBag)
    }
}

extension ResourceDetailViewModelImp: ResourceDetailViewModel {
    var resourceDetailItemViewModelsObservable: Observable<[ResourceDetailItemViewModel]> {
        return self.rx_resourceDetailItemViewModels.asObservable()
    }
    
    func reload() {
        self.resource.cancel()
        self.resource.start()
    }
    
    var resourceDetailItemViewModels: [ResourceDetailItemViewModel] {
        return self.rx_resourceDetailItemViewModels.value
    }
    
    var nameObservable: Observable<String> {
        return Observable.just(self.resource.name)
    }
}

