//
//  ViewController.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/9/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit
import SVProgressHUD
import RxSwift

class ViewController: UIViewController {
    
    let model: HomeViewModel
    
    init(model: HomeViewModel = HomeViewModelImp.defaultInstance) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
        
    }
    
    enum Id: String, NKViewIdentifier{
        case tableView
    }
    
    private lazy var tableView: NKTableView = Id.tableView.view(self)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var resetBarButton: UIBarButtonItem = {
//        let a = ActionImageButton.init
        
        return UIBarButtonItem.nk_create(with: ActionTitleButton(title: "Reset", color: UIColor.red, font: THEME.titleFont, target: self, selector: #selector(ViewController.tappedResetButton)))
    }()
    
    lazy var addBarButton: UIBarButtonItem = {
        return UIBarButtonItem.nk_create(with: ActionTitleButton(title: "Add", color: UIColor.black, font: THEME.titleFont, target: self, selector: #selector(ViewController.tappedAddButton)))
    }()
    
    lazy var pauseBarButton: UIBarButtonItem = {
        return UIBarButtonItem.nk_create(with: ActionTitleButton(title: "Pause", color: UIColor.black, font: THEME.titleFont, target: self, selector: #selector(ViewController.tappedPauseButton)))
    }()
    
    lazy var resumeBarButton: UIBarButtonItem = {
        return UIBarButtonItem.nk_create(with: ActionTitleButton(title: "Resume", color: UIColor.blue, font: THEME.titleFont, target: self, selector: #selector(ViewController.tappedResumeButton)))
    }()
    
    override func loadView() {
        super.loadView()
        
        self.view.nk_config {
            $0.backgroundColor = UIColor.white
        }
        .nk_addSubview(NKTableView().nk_id(Id.tableView)) {
            $0.backgroundColor = UIColor.white
            $0.register(cellType: ResourceItemViewCell.self)
            $0.tableFooterView = UIView()
            
            $0.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupRx()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.nk_classes(Class.Navigation.white)
            .nk_setTitle(title: "Files", color: UIColor.black, font: UIFont.systemFont(ofSize: 16))
        
        self.navigationItem.leftBarButtonItems = [self.addBarButton]
        
        self.resetBarButton.nk_view?.isHidden = true
    }
    
    func setupRx() {
        self.model.isLoadingObservable.observeOn(MainScheduler.instance).bindNext { (isLoading) in
            isLoading ? SVProgressHUD.show(with: SVProgressHUDMaskType.black) : SVProgressHUD.dismiss()
        }.addDisposableTo(self.nk_disposeBag)
        
        self.model.errorObservable.bindNext { (error) in
            SVProgressHUD.showError(withStatus: error, maskType: SVProgressHUDMaskType.black)
        }.addDisposableTo(self.nk_disposeBag)
        
        self.model.statusObservable.observeOn(MainScheduler.instance).bindNext { (status) in
            let leftButtons: [UIBarButtonItem]?
            let rightButtons: [UIBarButtonItem]?
            switch status {
            case .loading, .idle:
                leftButtons = [self.addBarButton]
                rightButtons = nil
            case .downloading:
                leftButtons = [self.resetBarButton]
                rightButtons = [self.pauseBarButton]
            case .paused:
                leftButtons = [self.resetBarButton]
                rightButtons = [self.resumeBarButton]
            }
            
            self.navigationItem.leftBarButtonItems = leftButtons
            self.navigationItem.rightBarButtonItems = rightButtons
        }.addDisposableTo(self.nk_disposeBag)
        
        self.model.resourceItemViewModelsObservable.observeOn(MainScheduler.instance).bindNext { (models) in
            self.tableView.reloadWithItems(items: models.map {$0 as Any})
        }.addDisposableTo(self.nk_disposeBag)
        
        self.tableView.onDidSelectItem = { any in
            guard let resourceItemViewModel = any as? ResourceItemViewModel else {
                return
            }
            let viewController = ResourceDetailViewController.init(model: ResourceDetailViewModelImp.init(resource: resourceItemViewModel.resource))
            self.navigationController?.pushViewController(viewController , animated: true)
        }
    }
    
    //MARK: Event
    func tappedResetButton() {
        self.model.reset()
    }
    
    func tappedAddButton() {
        self.model.add()
    }
    
    func tappedPauseButton() {
        self.model.pause()
    }
    
    func tappedResumeButton() {
        self.model.resume()
    }
}
