//
//  ResourceItemViewCell.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/11/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit
import NATTableView

class ResourceItemViewCell: NKBaseTableViewCell {
    struct Layout {
        static var Spacing: CGFloat {return 5}
        static var StackMargin: UIEdgeInsets {return UIEdgeInsets.init(top: 10, left: 10, bottom: 10, right: 10)}
        static var RecomendationHeight: CGFloat {return 80}
        static var ProgressHeight: CGFloat {return 3}
    }
    
    enum Id: String, NKViewIdentifier {
        case nameLabel, statusLabel, progressView
    }
    
    lazy var nameLabel: UILabel = Id.nameLabel.view(self)
    lazy var statusLabel: UILabel = Id.statusLabel.view(self)
    lazy var progressView: UIProgressView = Id.progressView.view(self)
    
    override func setupView() {
        self.nk_config {
            $0.layoutMargins = .zero
            $0.accessoryType = .disclosureIndicator
            $0.selectedBackgroundView = UIImageView(image: UIImage.nk_fromColor(UIColor.gray))
        }
        .nk_addSubview(UIStackView.nk_column()) {
            $0.distribution = .fillEqually
            $0.spacing = Layout.Spacing
            $0.layoutMargins = Layout.StackMargin
            $0.isLayoutMarginsRelativeArrangement = true
            $0.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })
            
            $0
            .nk_addArrangedSubview(UILabel().nk_id(Id.nameLabel).nk_classes(Class.Label.normal))
            .nk_addArrangedSubview(UILabel().nk_id(Id.statusLabel).nk_classes(Class.Label.subtitle))
        }
        .nk_addSubview(UIProgressView.init(progressViewStyle: UIProgressViewStyle.default).nk_id(Id.progressView).nk_classes(Class.Progress.gray)) {
            $0.snp.makeConstraints({ (make) in
                make.leading.trailing.bottom.equalTo(0)
                make.height.equalTo(Layout.ProgressHeight)
            })
        }
    }
}

extension ResourceItemViewCell: NKLayoutTestable {
    var size: CGSize {
        return CGSize.init(width: NKScreenSize.Current.width, height: Layout.RecomendationHeight)
    }
}

extension ResourceItemViewCell: NKLayoutModelable {
    typealias Model = ResourceItemViewModel
    static var models: [ResourceItemViewModel] {
        return [
//            ResourceItemViewModelImp.init( name: "image0", status: .downloading, progress: 0.3),
//            ResourceItemViewModelImp.init(name: "image2", status: .finished, progress: 1),
//            ResourceItemViewModelImp.init(name: "image3", status: .queue, progress: 0)
        ]
    }
    
    func config(_ model: ResourceItemViewModel) {
        self.nameLabel.text = model.name
        self.progressView.progress = model.progress
        self.statusLabel.text = model.status.string
    }
}

extension ResourceItemViewCell: ATTableViewCellProtocol {
    typealias ModelType = ResourceItemViewModelImp
    
    static func nibName() -> String? { return nil }
    
    func configureCell(model: ResourceItemViewModelImp) {
        self.config(model)
    }
    
    static func height(model: ResourceItemViewModelImp) -> CGFloat {
        return Layout.RecomendationHeight
    }
    
}

enum ResourceItemViewStatus {
    case downloading
    case finished
    case queue
    
    var string: String {
        switch self {
        case .downloading:
            return "Downloading..."
        case .finished:
            return "Finished"
        case .queue:
            return "Queuing..."
        }
    }
}

//MARK: Model
protocol ResourceItemViewModel {
    var name: String {get}
    var status: ResourceItemViewStatus {get}
    var progress: Float {get}
    var resource: IResource {get}
}

struct ResourceItemViewModelImp: ResourceItemViewModel {
    let name: String
    let status: ResourceItemViewStatus
    let progress: Float
    let resource: IResource
}

extension ResourceItemViewModelImp {
    init(resource: IResource, name: String, resourceStatus: ResourceStatus, progress: Float) {
        
        self.resource = resource
        self.name = name
        
        switch resourceStatus {
        case .downloading(_):
            self.status = .downloading
        case .finished:
            self.status = .finished
        case .idle:
            self.status = .queue
        case .paused:
            self.status = .queue
        }
        
        self.progress = progress
    }
}
