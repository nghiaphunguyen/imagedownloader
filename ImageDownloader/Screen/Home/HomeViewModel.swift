//
//  HomeViewModel.swift
//  ImageDownloader
//
//  Created by Nghia Nguyen on 12/11/16.
//  Copyright © 2016 Nghia Nguyen. All rights reserved.
//

import UIKit
import NKit
import RxSwift
import NRxSwift
import SwiftyJSON

enum HomeViewStatus {
    case idle, loading, downloading, paused
    
    static var stateFlow: [HomeViewStatus: [HomeViewStatus]] {
        return [.idle: [.downloading, .paused],
                .loading: [.idle],
                .downloading: [.loading, .paused],
                .paused: [.downloading]
        ]
    }
    
    func changeState(state: HomeViewStatus) -> HomeViewStatus {
        if (HomeViewStatus.stateFlow[state]?.contains(self) ?? false) == true {
            return state
        }
        
        return self
    }
}

protocol HomeViewModel {
    var resourceItemViewModelsObservable: Observable<[ResourceItemViewModel]> {get}
    var statusObservable: Observable<HomeViewStatus> {get}
    var isLoadingObservable: Observable<Bool> {get}
    var errorObservable: Observable<String> {get}
    func add()
    func reset()
    func resume()
    func pause()
}

class HomeViewModelImp: NSObject {
    enum HomeViewError: Error {
        case InvalidResult
    }
    
    fileprivate var rx_resources = Variable<[IResource]>([])
//    private var rx_resourceItemViewModels = Variable<[ResourceItemViewModel]>([])
    fileprivate var rx_status = Variable<HomeViewStatus>(.idle)
    fileprivate var rx_error = Variable<Error?>(nil)
    
    fileprivate let resourceDownloadee: IDownload
    fileprivate let downloader: Downloadable
    
    fileprivate var resourcesDisposeable: Disposable? = nil
    
    init(resourceDownloadee: IDownload, downloader: Downloadable) {
        self.resourceDownloadee = resourceDownloadee
        self.downloader = downloader
        super.init()

        self.resourceDownloadee.errorObservable.bindTo(self.rx_error)
            .addDisposableTo(self.nk_disposeBag)
    }
    
    fileprivate func createResources(from converterOuput: JSONSUnzipConverterOutput) {
        self.rx_resources.value = converterOuput
            .map { Resource.init(name: $0.0, downloadees: $0.1.arrayValue
                .flatMap {$0.string}
                .flatMap { Downloadee.create(from: $0, downloader: self.downloader) })
        }
        
        self.resourcesDisposeable?.dispose()
        self.resourcesDisposeable = Observable.from(self.rx_resources.value.map {$0.resourceStatusObservable}).merge().bindNext { (resource) in
            self.rx_resources.nk_reload()
        }
    }
}

extension HomeViewModelImp: HomeViewModel {
    func add() {
        guard self.rx_status.value == .idle else { return }
        
        self.resourceDownloadee.start()
        self.resourceDownloadee.resultObservable.take(1).observeOn(MainScheduler.instance).bindNext { (result) in
            self.createResources(from: result as! JSONSUnzipConverterOutput)
            self.start()
            }.addDisposableTo(self.nk_disposeBag)
        
        self.rx_status.value = self.rx_status.value.changeState(state: .loading)
    }
    
    private func start() {
        guard self.rx_status.value == .loading else {return}
        
        self.rx_resources.value.forEach({
            $0.start()
        })
        
        self.rx_status.value = self.rx_status.value.changeState(state: .downloading)
    }
    
    func reset() {
        guard self.rx_status.value == .downloading
            || self.rx_status.value == .paused else { return }
        
        self.resourceDownloadee.cancel()
        
        self.rx_resources.value.forEach({
            $0.cancel()
        })
        
        self.rx_resources.value = []
        
        self.rx_status.value = self.rx_status.value.changeState(state: .idle)
    }
    
    func resume(){
        guard self.rx_status.value == .paused else {
            return
        }
        
        self.rx_resources.value.forEach({
            $0.resume()
        })
        
        self.rx_status.value = self.rx_status.value.changeState(state: .downloading)
    }
    
    func pause() {
        guard self.rx_status.value == .downloading else {return}
        
        self.rx_resources.value.forEach({
            $0.pause()
        })
        
        self.rx_status.value = self.rx_status.value.changeState(state: .paused)
    }
    
    var resourceItemViewModelsObservable: Observable<[ResourceItemViewModel]> {
        return self.rx_resources.asObservable().map {
            
            return $0.map { ResourceItemViewModelImp(resource: $0, name: $0.name, resourceStatus: $0.state, progress: $0.progress)}
        }
    }
    
    var statusObservable: Observable<HomeViewStatus> {
        return self.rx_status.asObservable()
    }
    
    var isLoadingObservable: Observable<Bool> {
        return self.statusObservable.map {$0 == .loading}
    }
    
    var errorObservable: Observable<String> {
        return self.rx_error.asObservable().nk_unwrap().map {"Occurred error \($0)"}
    }
}

extension HomeViewModelImp {
    static var defaultInstance: HomeViewModelImp {
        let downloader = Downloader.init()
        let resourceDownloadee = Downloadee.init(url: URL(string: "https://dl.dropboxusercontent.com/u/4529715/JSON%20files%20updated.zip")!, downloader: downloader, converter: JSONSUnzipConverter())
        return HomeViewModelImp.init(resourceDownloadee: resourceDownloadee, downloader: downloader)
    }
}
